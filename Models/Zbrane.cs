﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using Xamarin.Forms;

namespace App2.Models
{
    public class Zbrane
    {
        [PrimaryKey]
        public int id_zbrane { get; set; }
        public string Nazev { get; set; }
        public int Pocet { get; set; }
        public string Cena { get; set; }
        public string PoskS { get; set; }
        public string PoskM { get; set; }
        public string Kriticky { get; set; }
        public string Dostrel { get; set; }
        public string Vaha { get; set; }
        public string Typ { get; set; }
        public string Special1 { get; set; }
        public string Special2 { get; set; }
        public string Special3 { get; set; }
        public string Kat { get; set; }
        public string PodKat { get; set; }
        public int CenaInt { get; set; }
        public string Popis { get; set; }
        public Byte[] obrazek { get; set; }
        
    }
}
